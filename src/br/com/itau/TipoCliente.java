package br.com.itau;

public enum TipoCliente {
    PERSONALITE, UNICLASS, PRIVATE, VAREJO
}