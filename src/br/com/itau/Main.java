package br.com.itau;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        // write your code here

        try {
            Cliente cliente = new Cliente(
                    "Bruno Manoel",
                    16,
                    "331.764.288-20",
                    TipoCliente.PRIVATE);

            Conta conta = new Conta(
                    12, 1001, 1000, cliente
            );
            conta.sacar(10);
            System.out.println(conta.getSaldo());
        }
        catch (ArithmeticException e){
            System.out.println(e);
        }
    }
}