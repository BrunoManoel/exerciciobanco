package br.com.itau;

public class Cliente {
    private String nome;
    private int idade;
    private String cpf;
    private TipoCliente tipoCliente;

    public Cliente(String nome, int idade, String cpf, TipoCliente tipoCliente) {
        if(idade < 18){
            throw new ArithmeticException("Ops, você é de menor");
        }
        this.nome = nome;
        this.idade = idade;
        this.cpf = cpf;
        this.tipoCliente = tipoCliente;
    }

    public Cliente(){}

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public TipoCliente getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(TipoCliente tipoCliente) {
        this.tipoCliente = tipoCliente;
    }
}
